package io.turntabl.accramegastore.services.recommendation;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class ComplementaryService {
    private final Map<String, Set<String>> complementaryProducts;

    public ComplementaryService(String filename) {
        complementaryProducts = loadComplementaryProducts(filename);
    }

    private Map<String, Set<String>> loadComplementaryProducts(String filename) {
        Map<String, Set<String>> temp = new HashMap<>();
        List<String> lines;

        try {
            URL systemResource = ClassLoader.getSystemResource(filename);
            lines = Files.readAllLines(Paths.get(systemResource.toURI()));
            lines.remove(0);
        }

        catch (URISyntaxException | IOException e) {
            throw new RuntimeException("failed to load products");
        }

        for (String line : lines) {
            String[] values = line.split(",");

            String key = values[0].trim();
            String value = values[1].trim();

            temp.putIfAbsent(key, new HashSet<>());
            temp.putIfAbsent(value, new HashSet<>());
            temp.get(key).add(value);
            temp.get(value).add(key);
        }
        return Map.copyOf(temp);
    }

     protected Map<String, Set<String>> getProductIds() {
        return complementaryProducts;
    }

    public Set<String> getComplements(String productionId) {
        return complementaryProducts.getOrDefault(productionId, new HashSet<>());
    }
}
